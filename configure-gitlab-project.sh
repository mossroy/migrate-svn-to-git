#!/bin/bash
if [[ "$#" -ne 3 ]] && [[ "$#" -ne 4 ]]; then
    echo "This script takes 3 or 4 parameters : the Gitlab Private Token (generate it in the user settings of gitlab), the gitlab numeric id of the project, the password of gitlab account in JIRA, et (optional) the Jenkins token to automatically launch Jenkins builds on Merge Requests"
    echo "Example :"
    echo "  ./configure-gitlab-project.sh gitlabToken 34 gitlabPasswordOnJira jenkinsToken"
    exit 2
fi

GITLAB_URL="https://gitserver/api/v4/"
PRIVATE_TOKEN=$1
PROJECT_ID=$2
JIRA_GITLAB_PASSWORD=$3
JENKINS_TOKEN=$4

echo "Configuring the project : disable gitlab issues and enable Request access"
curl --silent --insecure --header "Private-Token: $PRIVATE_TOKEN" --request PUT --form "issues_enabled=false" --form "request_access_enabled=true" $GITLAB_URL/projects/$PROJECT_ID

echo
echo "--------------------"
echo "Configuring protected branch on master"
curl --silent --insecure --header "Private-Token: $PRIVATE_TOKEN" --request DELETE $GITLAB_URL/projects/$PROJECT_ID/protected_branches/master
curl --silent --insecure --header "Private-Token: $PRIVATE_TOKEN" --request POST --form "name=master" --form "merge_access_level=30" --form "push_access_level=40" $GITLAB_URL/projects/$PROJECT_ID/protected_branches

echo
echo "--------------------"
echo "Configuring integration with JIRA"
curl --silent --insecure --header "Private-Token: $PRIVATE_TOKEN" --request PUT --form "url=https://jiraserver/" --form "active=true" --form "jira_issue_transition_id=5" --form "commit_events=false" --form "merge_requests_events=false" --form "username=gitlab" --form "password=$JIRA_GITLAB_PASSWORD" $GITLAB_URL/projects/$PROJECT_ID/services/jira

echo
echo "--------------------"
if [[ ! -z "$JENKINS_TOKEN" ]]; then
    if curl --silent --insecure --header "Private-Token: $PRIVATE_TOKEN" $GITLAB_URL/projects/$PROJECT_ID/hooks | grep webpic; then
        echo "Jenkins integration is already configured"
    else
        echo "Configuring integration with Jenkins"
        curl --silent --insecure --header "Private-Token: $PRIVATE_TOKEN" --request POST --form "url=https://jenkinsserver/gitlab-webhook/post" --form "merge_requests_events=true" --form "enable_ssl_verification=false" --form "token=$JENKINS_TOKEN" --form "push_events=false" $GITLAB_URL/projects/$PROJECT_ID/hooks
    fi
else
    echo "No change on Jenkins integration because the parameter has not been passed"
fi
