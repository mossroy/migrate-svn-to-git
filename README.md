# migrate-svn-to-git

Scripts to migrate a subversion repository to git, and configure a gitlab project through its APIs.

Read https://blog.mossroy.fr/2019/10/19/migration-subversion-vers-gitlab/ (French blog post) for more context information

## Script migrate-svn-to-git.sh

Origin of the content : https://gist.github.com/mossroy/22aebd54133b17e0b70283fd50bb616a (forked from https://gist.github.com/epicserve/1219858)

This script takes 3 parameters : the SVN URL (directory above the trunk/tags/branches), the git URL, and a working directory.
It can be run from Git Bash (on Windows) or from Linux (tested on CentOS 7, but should work on many other systems).
It works with both git 1.x and git 2.x.

It must be started from the directory where it's located, because it will look for file final-authors.txt in the same local directory.

This final-authors.txt file must be populated with the list of ALL authors that commited to SVN, with their corresponding e-mail in the target git repository.
It's advisable to check the content of the .gitattributs-force-lf-line-endings for extensions that would not be handled properly before running the script (consider it as a sample)

Example :

    ./migrate-svn-to-git.sh https://svnserver/repo/directory-above-trunk https://gitserver/namespace/reponame.git /d/temp/svn-to-git-migrations/reponame

A few things to know :

The script will overwrite any existing .gitignore file with the conversion of svn:ignore properties. In some circumstances, I noticed that the generated .gitignore file could have lines like `directory/*`, which you would probably prefer to remove. So check this file before actually using the git repo.
It will also overwrite any existing .gitattributes file. Put it back if necessary

On a very big SVN repo, the migration failed on Git Bash (Windows) with the following error message : `cannot create file ... Invalid Argument`. My workaround has been to run the script on Linux in this case.

## Script configure-gitlab-project.sh

This script is more specific than the previous one, and should probably be adapted to the context. It protects the master branch, enables Request Access, disables issues of gitlab, and replace them with an integration with JIRA. It also optionally adds integration with Jenkins.

This script takes 3 or 4 parameters : the Gitlab Private Token (generate it in the user settings of gitlab), the gitlab numeric id of the project, the password of gitlab account in JIRA, et (optional) the Jenkins token to automatically launch Jenkins builds on Merge Requests

Example :

    ./configure-gitlab-project.sh gitlabToken 34 gitlabPasswordOnJira jenkinsToken
